import org.junit.jupiter.api.Test;
import sun.tools.jconsole.Plotter;

import static org.junit.jupiter.api.Assertions.*;

class PolinomTest {

    @Test
    void addPolinomials() {
        Polinom f=new Polinom();
        Polinom g=new Polinom();
        Polinom result = new Polinom();
        Monom m1=new Monom(1,2);
        Monom m2= new Monom(0,1);
        f.getPolinom().add(m1);
        f.getPolinom().add(m2);
        Monom m3=new Monom(1,2);
        g.getPolinom().add(m3);
        Monom m4=new Monom(1,4);
        result.getPolinom().add(m4);
        result.getPolinom().add(m2);
        Polinom calculated=f.addPolinomials(g);
        assertEquals(result,calculated);
    }

    @Test
    void substrPolinomials() {
    }

    @Test
    void dividePolinomials() {
    }

    @Test
    void multiplyPolinomials() {
    }

    @Test
    void integration() {
    }

    @Test
    void derivation() {
    }
}