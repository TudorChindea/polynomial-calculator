import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.event.ActionEvent;

import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.lang.Float;
import java.lang.Integer;

public class Controller {
    @FXML
    private Button btn1;
    @FXML
    private Button btn2;
    @FXML
    private Button btn3;
    @FXML
    private Button btn4;
    @FXML
    private Button btn5;
    @FXML
    private Button btn6;
    @FXML
    private TextField polin1;
    @FXML
    private TextField polin2;
    @FXML
    private TextField result;
    @FXML
    private TextField result2;
        @FXML
    private TextField invalidInput;
        @FXML
    private TextField invalidInput1;

    @FXML
    private Button btn7;
    public String input1,input2 = new String();
    private Polinom poli1= new Polinom();
    private Polinom poli2 = new Polinom();
    void printPolinom(Polinom polinom, TextField result2){
        result2.setText("");
        String output="";
        for(Monom monom: polinom.getPolinom())
        {
            if(monom.getCoeff()>0)
                output=output+"+"+String.valueOf(monom.getCoeff()) ;
            else if(monom.getCoeff() < 0)
                output=output+String.valueOf(monom.getCoeff()) ;
            if(monom.getDegree()>0 && monom.getCoeff() != 0)
                output=output + "x^" + String.valueOf(monom.getDegree());
        }
        if(!output.equals("")) {
            if (output.charAt(0) == '+')
                output = output.substring(1);
            if (output.length() != 0)
                result2.setText(output);
        }
        else
            result2.setText("0");
    }
    @FXML
    void addPolinom(ActionEvent event) {
        result2.clear();
        Polinom result1 = new Polinom();
        result1 = poli1.addPolinomials(poli2);
        printPolinom(result1,result);
    }
    @FXML
    void derivatePolinom(ActionEvent event) {
        result2.clear();
        Polinom result1 = new Polinom();
        result1 = poli1.derivation();
        printPolinom(result1,result);
    }
    @FXML
    void dividePolinom(ActionEvent event) {
        ArrayList<Polinom> result1 = new ArrayList<>();
        result1=poli1.dividePolinomials(poli2);
        printPolinom(result1.get(1),result);
        printPolinom(result1.get(0),result2);


    }
    Polinom validation( String input, TextField warningT, TextField getPolin){
        Polinom polin = new Polinom();
        input = getPolin.getText();
        String noSpaceStr=input.replaceAll("\\s+","");
        String regx="(([+-]?([0-9]([0-9]+)?)?(x(\\^[1-9]([0-9]+)?)?)?)+)";
        Pattern p = Pattern.compile(regx);
        Matcher m = p.matcher( noSpaceStr );
        if (p.matcher(noSpaceStr).matches() ) {
            warningT.setText("");
            int index = 0, deg;
            float coeff;
            String newPoly = noSpaceStr.replaceAll("-", "+-");
            StringTokenizer stk = new StringTokenizer(newPoly, "+");
                while (stk.hasMoreElements()) {
                    String mono= stk.nextToken();
                    coeff = 0;
                    deg = 0;
                    int xPos = mono.indexOf("x");
                    int degPos = mono.indexOf("^");
                    if (xPos != -1) {
                        if (xPos == 0) {
                            coeff = 1;
                        } else if (xPos == 1 && mono.charAt(0) == '-') {
                            coeff = -1;
                        } else {
                            coeff = Float.parseFloat(mono.substring(0, xPos));
                        }
                    } else {
                        coeff = Float.parseFloat(mono);
                        deg = 0;
                    }
                    if (degPos != -1) {
                        deg = Integer.parseInt(mono.substring(degPos + 1));
                    } else if (xPos != -1) {
                        deg = 1;
                    }
                    Monom m1=new Monom(deg,coeff);
                    polin.getPolinom().add(m1);
                    index++;
                }

        }
        else{
                warningT.setText("Invalid Polinom");
            }
        return polin;
    }
    @FXML
    void getPolinoms(ActionEvent event) {

        poli1=validation( input1, invalidInput, polin1);
        poli2 =validation( input2, invalidInput1, polin2);
    }
    @FXML
    void integratePolinom(ActionEvent event) {
        result2.clear();
        Polinom result1 = new Polinom();
        result1 = poli1.integration();
        printPolinom(result1,result);
    }
    @FXML
    void multiplyPolinom(ActionEvent event) {
        result2.clear();
        Polinom result1 = new Polinom();
        result1=poli1.multiplyPolinomials(poli2);
        printPolinom(result1,result);
    }
    @FXML
    void substractPolinom(ActionEvent event) {
        result2.clear();
        Polinom result1 = new Polinom();
        result1 = poli1.substrPolinomials(poli2);
        printPolinom(result1,result);
    }
}